# This is the source.sh script. It is executed by BPM in a temporary directory when compiling a source package
# BPM Expects the source code to be extracted into the automatically created 'source' directory which can be accessed using $BPM_SOURCE
# BPM Expects the output files to be present in the automatically created 'output' directory which can be accessed using $BPM_OUTPUT

DOWNLOAD="https://www.cpan.org/src/5.0/perl-${BPM_PKG_VERSION}.tar.xz"
FILENAME="${DOWNLOAD##*/}"

# The prepare function is executed in the root of the temp directory
# This function is used for downloading files and putting them into the correct location
prepare() {
  wget "$DOWNLOAD"
  tar -xvf "$FILENAME" --strip-components=1 -C "$BPM_SOURCE"
}

# The build function is executed in the source directory
# This function is used to compile the source code
build() {
  export BUILD_ZLIB=False
  export BUILD_BZIP2=0
  sh Configure -des                                         \
               -Dprefix=/usr                                \
               -Dvendorprefix=/usr                          \
               -Dprivlib=/usr/lib/perl5/5.38/core_perl      \
               -Darchlib=/usr/lib/perl5/5.38/core_perl      \
               -Dsitelib=/usr/lib/perl5/5.38/site_perl      \
               -Dsitearch=/usr/lib/perl5/5.38/site_perl     \
               -Dvendorlib=/usr/lib/perl5/5.38/vendor_perl  \
               -Dvendorarch=/usr/lib/perl5/5.38/vendor_perl \
               -Dman1dir=/usr/share/man/man1                \
               -Dman3dir=/usr/share/man/man3                \
               -Dpager="/usr/bin/less -isR"                 \
               -Duseshrplib                                 \
               -Dusethreads
  make
}

# The check function is executed in the source directory
# This function is used to run tests to verify the package has been compiled correctly
check() {
  make test_harness
}

# The package function is executed in the source directory
# This function is used to move the compiled files into the output directory
package() {
  make DESTDIR="$BPM_OUTPUT" install
}
